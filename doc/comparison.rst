Servers comparison
##################

Here is a feature comparison with other OpenID Connect server software.

Canaille voluntarily only implements the OpenID Connect protocol to keep its codebase simple.
We are currently working on supporting SQL databases backends.

+---------------+-------+-----------+------+---------------------------+--------------+
| Software      | Project                  | Protocols implementations | Backends     |
|               +-------+-----------+------+------+------+-------------+------+-------+
|               | FLOSS | Language  | LOC  | OIDC | SAML | CAS         | LDAP | SQL   |
+===============+=======+===========+======+======+======+=============+======+=======+
| Canaille      | ✅    | Python    | 10k  | ✅   | ❌   | ❌          | ✅   | ❌    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Auth0`_      | ❌    | ❔        | ❔   | ✅   | ✅   | ❌          | ✅   | ❔    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Authelia`_   | ✅    | Go        | 50k  | ✅   | ❌   | ❌          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Authentic2`_ | ✅    | Python    | 65k  | ✅   | ✅   | ✅          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Authentik`_  | ✅    | Python    | 55k  | ✅   | ✅   | ❌          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `CAS`_        | ✅    | Java      | 360k | ✅   | ✅   | ✅          | ✅   | ❌    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Connect2id`_ | ❌    | ❔        | ❔   | ✅   | ✅   | ❌          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Gluu`_       | ✅    | Java      | ❔   | ✅   | ✅   | ✅          | ✅   | ❔    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Hydra`_      | ✅    | Go        | 50k  | ✅   | ✅   | ❌          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Keycloak`_   | ✅    | Java      | 600k | ✅   | ✅   | ✅          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `LemonLDAP`_  | ✅    | Perl      | 130k | ✅   | ✅   | ✅          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+
| `Okta`_       | ❌    | ❔        | ❔   | ✅   | ✅   | ❌          | ✅   | ✅    |
+---------------+-------+-----------+------+------+------+-------------+------+-------+

.. _Auth0: https://auth0.com
.. _Authelia: https://authelia.com
.. _Authentic2: https://dev.entrouvert.org/projects/authentic
.. _Authentik: https://goauthentik.io
.. _CAS: https://apereo.github.io/cas
.. _Connect2id: https://connect2id.com
.. _Gluu: https://gluu.org
.. _Hydra: https://ory.sh
.. _Keycloak: https://keycloak.org
.. _LemonLDAP: https://lemonldap-ng.org
.. _Okta: https://okta.com
